## miniLookup!

miniLookup! -program is quick and small program to check current status of the server.  
Program fetches image of current stats from the NWPublic.eu -webserver and displays it in tkinter GUI.

Program has to be restarted to fetch new image and display time when image was fetched.