import requests
import re
import tkinter as tk
from PIL import Image, ImageTk
from io import BytesIO
from time import strftime

#
# Simple Python program to see Minisiege -server stats.
#

def timenow():  # Time function
    time = strftime("%d/%m/%Y - %H:%M") # Assign current time to variable called 'time'
    return time

def statsimage():  # To fetch image from nwpublic.eu web server
    url = "https://status.nwpublic.eu/?s=mini"  # Url where to fetch
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))  # Img variable after fetching
    return img

# Create GUI with tkinter
window = tk.Tk()
window.title("miniLookup!")  # Window title
window.geometry("425x164")  # 425x164
window.resizable(0, 0)  # Resizable? No!
window['background'] = '#323232'  # Fixed background colour for the GUI

statusimage = ImageTk.PhotoImage(statsimage())  # Variable for getting Tkinter widget of the image
time = timenow()  # Variable to get time from function 'timenow'

status = tk.Label(image=statusimage, background='#323232').place(x=1, y=1)
timelbl = tk.Label(window, background='#323232', foreground='#fff', font='Verdana', text=f'LOCAL TIME: {time}').place(x=13, y=138)

# Create main loop for window
window.mainloop()